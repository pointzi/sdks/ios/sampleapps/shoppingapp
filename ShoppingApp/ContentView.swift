//
//  ContentView.swift
//  ShoppingApp
//
//  Created by Ganesh Faterpekar on 13/3/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
       Home()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
