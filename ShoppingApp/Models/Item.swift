//
//  Item.swift
//  ShoppingApp
//
//  Created by Ganesh Faterpekar on 23/3/21.
//

import SwiftUI


struct Item: Identifiable {
    let id = UUID().uuidString
    let title: String
    let subTitle: String
    let price: String
    let rating: String
    let image: String
}

var items = [
    Item(title: "Vincent Chase", subTitle: "Black Full Rim", price: "$36", rating: "3.8",image: "p1"),
    Item(title: "John Jacobs", subTitle: "Brown Tortoise", price: "$45", rating: "4.9",image: "p2"),
    Item(title: "Wood Black", subTitle: "Office Glass", price: "$84", rating: "4.2",image: "p3"),
    Item(title: "Vu Abstact", subTitle: "Fashion U", price: "$65", rating: "3.5",image: "p4"),
]
