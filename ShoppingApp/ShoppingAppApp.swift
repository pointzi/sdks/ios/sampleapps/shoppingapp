//
//  ShoppingAppApp.swift
//  ShoppingApp
//
//  Created by Ganesh Faterpekar on 13/3/21.
//

import SwiftUI

//@main
struct ShoppingAppApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
