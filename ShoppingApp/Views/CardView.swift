//
//  CardView.swift
//  ShoppingApp
//
//  Created by Ganesh Faterpekar on 25/3/21.
//

import SwiftUI

struct CardView: View {
    var item: Item
    var animation: Namespace.ID
    var body: some View {
        VStack {
            HStack {
                Spacer(minLength: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/)
                Text(item.price)
                    .priceModifier()
            }
            
            Image(item.image)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .matchedGeometryEffect(id: "image\(item.id)", in: animation)
                .padding()
            
            Text(item.title)
                .fontWeight(.bold)
                .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
            
            Text(item.subTitle)
                .fontWeight(.light)
                .font(.caption)
            
            HStack {
                Button(action: {}, label: {
                    Image(systemName: "suit.heart")
                        .foregroundColor(.black)
                })
                .matchedGeometryEffect(id: "heart\(item.id)", in: animation)
                
                Spacer(minLength: 0)
                
                Text(item.rating)
                    .matchedGeometryEffect(id: "rating\(item.id)", in: animation)
                  
            }
        }
        .padding()
        .background(Color(item.image))
        .matchedGeometryEffect(id: "color\(item.id)", in: animation)
        .cornerRadius(15.0)
    }
}


//struct CardView_Previews: PreviewProvider {
//    static var previews: some View {
//        CardView(item: items[0], animation: )
//    }
//}

struct PriceModifier: ViewModifier {
    let fontbold = Font.system(size: 24).weight(.semibold)
    func body(content: Content) -> some View {
          content
            .font(fontbold)
            .padding()
            .background(Color.white.opacity(0.5))
    }

}


extension View {
    func priceModifier() -> some View {
        modifier(PriceModifier())
    }
}
