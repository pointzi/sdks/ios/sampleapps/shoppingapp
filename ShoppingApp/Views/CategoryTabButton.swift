//
//  CategoryTabButton.swift
//  ShoppingApp
//
//  Created by Ganesh Faterpekar on 14/3/21.
//

import SwiftUI

struct CategoryTabButton: View {
    var title: String
    var animation: Namespace.ID
    @Binding var selected: String

    var body: some View {
        Button(action: {
            withAnimation(.spring()) {
                selected = title
            }
        }){
            Text(title)
                .font(.system(size: 15))
                .foregroundColor( selected == title ? .white : .black)
                .padding()
                .background(
                    ZStack {
                    if selected == title{
                        Color.black
                            .clipShape(Capsule())
                            .matchedGeometryEffect(id: "Tab", in: animation)
                    }
                })
            
            
        }
    }
}

