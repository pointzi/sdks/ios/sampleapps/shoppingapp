//
//  DetailView.swift
//  ShoppingApp
//
//  Created by Ganesh Faterpekar on 4/4/21.
//

import SwiftUI

struct DetailView: View {
    @Binding var selectedItem: Item
    @Binding var show: Bool
    var animation: Namespace.ID
    @State var selectedColor : Color = Color("p1")
    
    var body: some View {
     VStack {
        HStack() {
            Button(action: {
                withAnimation(.spring()) {
                    show.toggle()
                }
            }, label: {
                getImageicon(name: "chevron.left")
            })
            Spacer()
            Button(action: {}, label: {
                getImageicon(name: "magnifyingglass")
               
            })
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                getImageicon(name: "bag")
            })
        }
        
        VStack{
            Image(selectedItem.image)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .padding()
                .matchedGeometryEffect(id: "image\(selectedItem.id)", in: animation)
            
            Text(selectedItem.title)
                .font(.title)
            Text(selectedItem.subTitle)
                .font(.caption)
            
            HStack{
                Text(selectedItem.rating)
                    .matchedGeometryEffect(id: "rating\(selectedItem.id)", in: animation)
                Spacer(minLength: 0)
                
                
                Button(action: {}, label: {
                    Image(systemName: "suit.heart")
                        .foregroundColor(.black)
                })
                .matchedGeometryEffect(id: "heart\(selectedItem.id)", in: animation)
                
            }.padding()
        }.background(Color(selectedItem.image)
                        .clipShape(CustomArcShape()))
        .cornerRadius(15)
        .padding()
        
        VStack(alignment: .leading,spacing:8) {
            
            Text("Exclusive Offer")
                .fontWeight(.heavy)
                .foregroundColor(.black)
            
            Text("Frame  + Lens for $35(it's 50% off)")
                .foregroundColor(.gray)
            
            
        }
        .padding()
        .frame(width: UIScreen.main.bounds.width - 30, alignment: .leading)
        .background(Color("p3"))
        .cornerRadius(15.0)
        
    
        VStack(alignment:.leading,spacing:10) {
            Text("Color")
                .fontWeight(.heavy)
                .foregroundColor(.black)
            HStack(spacing:15) {
                ForEach(1...4, id:\.self) { i in
                    ZStack {
                      Color("p\(i)")
                        .clipShape(Circle())
                        .frame(width: 45, height: 45)
                        .onTapGesture {
                            withAnimation (.spring()){
                                selectedColor = Color("p\(i)")
                         }}
                        
                        if selectedColor == Color("p\(i)"){
                            Image(systemName: "checkmark")
                                .foregroundColor(.black)
                        }
                    }
                }
                Spacer(minLength: 0)
            }
            
            VStack{
                
                Button(action: {}, label: {
                    Text("Try frame is 3D")
                        .fontWeight(.bold)
                        .frame(width:UIScreen.main.bounds.width - 100)
                        .padding()
                        .foregroundColor(.black)
                        .background (
                         RoundedRectangle(cornerRadius: 15)
                            .stroke(Color.black,lineWidth: 1)
                        )
                        .cornerRadius(15)
                })
                
                Button(action: {}, label: {
                    Text("Add to the cart")
                        .fontWeight(.bold)
                        .frame(width:UIScreen.main.bounds.width - 100)
                        .padding()
                        .foregroundColor(.white)
                        .background (Color.black)
                        .cornerRadius(15)
                }).padding()
               
            }
        }
        .cornerRadius(15.0)
        .padding()

        Spacer()
      }
    }
    
    fileprivate func getImageicon(name: String) -> some View {
        return Image(systemName: name)
            .font(.title)
            .foregroundColor(.black)
    }
}

struct DetailView_Previews: PreviewProvider {

    @State static var isShowing = false
    @State static var selectedItem : Item = items[0]
    @Namespace static var animation
    static var previews: some View {
        DetailView(selectedItem: $selectedItem, show: $isShowing, animation: animation)
    }
}
