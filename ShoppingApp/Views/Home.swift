//
//  Home.swift
//  ShoppingApp
//
//  Created by Ganesh Faterpekar on 13/3/21.
//

import SwiftUI

struct Home: View {
    
    @State var selected = tabs[0]
    @State var selectedItem : Item = items[0]
    @State var show: Bool = false
    
    @Namespace var animation
    var body: some View {
        ZStack {
        VStack{
            HStack{
                Button(action: {}){
                    Image(systemName: "line.horizontal.3.decrease")
                        .font(.system(size: 25, weight: .heavy))
                        .foregroundColor(Color.black)
                }
                Spacer()
                
                Button(action:{}) {
                    Image("profile")
                        .resizable()
                        .frame(width: 45, height: 45)
                        .cornerRadius(15)
                        .aspectRatio(contentMode: .fill)
                }
            }
            .padding(.vertical,10)
            .padding(.horizontal,10)
            
            Spacer(minLength: 0)
            
            ScrollView {
                VStack {
                    HStack {
                      
                        VStack (alignment:.leading,spacing: 5) {
                            Text("Lets Go")
                                .font(.title)
                                .foregroundColor(.black)
                            
                            Text("Get Started")
                                .font(.largeTitle)
                                .foregroundColor(.black)
                                .fontWeight(.heavy)
                        }
                        .padding(.horizontal)
                        Spacer(minLength: 0)
                        
                    }
                    HStack (spacing: 0) {
                        ForEach(tabs, id: \.self) { tab in
                            CategoryTabButton(title: tab, animation: animation, selected: $selected)
                            if tabs.last != tab {
                                Spacer(minLength: 0)
                            }
                        }
                    }
                    LazyVGrid(columns: Array(repeating: GridItem(.flexible(), spacing: 20), count: 2), alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: 20, pinnedViews: /*@START_MENU_TOKEN@*/[]/*@END_MENU_TOKEN@*/, content: {
                        ForEach(items) { item in
                             CardView(item: item, animation: animation)
                                .onTapGesture {
                                    withAnimation(.spring()) {
                                        selectedItem = item
                                        show.toggle()
                                    }
                                    
                                }
                        }
                    })
                }.padding()
                
                
                
            }
                
        }.background(Color.white)
        .opacity(show ? 0 : 1)
        
        if (show) {
            DetailView(selectedItem: $selectedItem,show: $show,animation: animation)
        }
    }
    }
}

struct Home_Previews: PreviewProvider {
    static var previews: some View {
        Home()
    }
}


var tabs = ["Glases","Watches","Shoes","Perfume"]
