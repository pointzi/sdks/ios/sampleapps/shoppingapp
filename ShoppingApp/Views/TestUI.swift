//
//  TestUI.swift
//  ShoppingApp
//
//  Created by Ganesh Faterpekar on 5/4/21.
//

import SwiftUI

struct TestUI: View {
    var body: some View {
        ZStack {
            
            CustomArcShape()
               // .stroke(Color.red, style: StrokeStyle(lineWidth: 10, lineCap: .round, lineJoin: .round))
                .frame(width: 300, height: 300)
        }
    }
}
struct CustomBgShape: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        path.move(to: CGPoint(x:rect.maxX,y:rect.maxY))
        path.addLine(to: CGPoint(x:rect.maxX,y:rect.minY))
        path.addLine(to: CGPoint(x:rect.minX,y:rect.maxY - 100))
        path.addLine(to: CGPoint(x:rect.minX,y:rect.maxY))
        path.addLine(to: CGPoint(x:rect.maxX,y:rect.maxY))
        
        return path
    }
    
    
}

struct CustomArcShape: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()
        
        
        let pt1 = CGPoint(x:rect.maxX,y:rect.maxY)
        let pt2 =  CGPoint(x:rect.maxX,y:rect.minY)
        let pt3 = CGPoint(x:rect.minX,y:rect.maxY - 100)
        let pt4 = CGPoint(x:rect.minX,y:rect.maxY)
        
        path.move(to: pt4)
        
        path.addArc(tangent1End: pt1, tangent2End: pt2, radius: 20)
        path.addArc(tangent1End: pt2, tangent2End: pt3, radius: 20)
        path.addArc(tangent1End: pt3, tangent2End: pt4, radius: 20)
        path.addArc(tangent1End: pt4, tangent2End: pt1, radius: 20)
        
        return path
    }
    
    
}

struct Triangle: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()

        path.move(to: CGPoint(x: rect.midX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.minY))

        return path
    }
}


struct TestUI_Previews: PreviewProvider {
    static var previews: some View {
        TestUI()
    }
}
